console.log( "game.js" );

player = {
    x: 640/2,
    y: 640/2,
    width: 16,
    height: 16,
    imageWidth: 64,
    imageHeight: 64,
    speed: 10,
    frameDir: 0,
    direction: "LEFT",
    image: null,
    
    Move: function( dir ) {
        if ( dir == "UP" ) {
            this.y -= this.speed;
        }
        else if ( dir == "DOWN" ) {
            this.y += this.speed;
        }
        else if ( dir == "LEFT" ) {
            this.x -= this.speed;
        }
        else if ( dir == "RIGHT" ) {
            this.x += this.speed;
        }
    },

    Turn: function( dir ) {
        // Can't go backwards
        if (    ( dir == "UP" && this.direction != "DOWN" ) ||
                ( dir == "DOWN" && this.direction != "UP" ) ||
                ( dir == "LEFT" && this.direction != "RIGHT" ) ||
                ( dir == "RIGHT" && this.direction != "LEFT" ) )
        {
            this.direction = dir;
        }

        if ( this.direction == "LEFT" )
        {
            this.frameDir = 0 * this.height;
        }
        else if ( this.direction == "RIGHT" )
        {
            this.frameDir = 1 * this.height;
        }
        else if ( this.direction == "UP" )
        {
            this.frameDir = 2 * this.height;
        }
        else if ( this.direction == "DOWN" )
        {
            this.frameDir = 3 * this.height;
        }
    },

    Update: function( timer ) {
        if ( timer % this.speed == 0 ) {
            this.Move( this.direction );
        }
    },

    Draw: function( canvas ) {
        canvas.drawImage( this.image,
            0, this.frameDir, this.width, this.height,          // frame clip
            this.x, this.y,                         // position
            this.width, this.height );              // scaled size
    }
};

gameState = {
    canvas: null,
    options: {},
    images: {},
    isDone: false,
    background : null,
    level : 1,
    startTimer : 60,
    timer : 0,

    objPlayer : null,
    
    keys: {
        UP:         { code: "w", isDown: false }, 
        DOWN:       { code: "s", isDown: false },
        RIGHT:      { code: "d", isDown: false },
        LEFT:       { code: "a", isDown: false },
        SHOOT:      { code: 32, isDown: false },
    },

    Init: function( canvas, options ) {
        gameState.canvas = canvas;
        gameState.options = options;
        gameState.isDone = false;

        // Load images
        gameState.background = new Image();
        gameState.background.src = "assets/images/background.png";

        gameState.images.player = new Image();
        gameState.images.player.src = "assets/images/kato.png";

        // Create entities
        objPlayer = player;
        objPlayer.image = gameState.images.player;
        console.log( objPlayer );
    },

    Clear: function() {
    },

    Click: function( ev ) {
    },

    KeyPress: function( ev ) {
        $.each( gameState.keys, function( i, key ) {
            if ( ev.key == key.code ) {
                key.isDown = true;
            }
        } );
    },

    KeyRelease: function( ev ) {
        $.each( gameState.keys, function( i, key ) {
            if ( ev.key == key.code ) {
                key.isDown = false;
            }
        } );
    },

    Update: function() {
        if ( gameState.keys.UP.isDown ) {
            objPlayer.Turn( "UP" );
        }
        else if ( gameState.keys.DOWN.isDown ) {
            objPlayer.Turn( "DOWN" );
        }
        if ( gameState.keys.LEFT.isDown ) {
            objPlayer.Turn( "LEFT" );
        }
        else if ( gameState.keys.RIGHT.isDown ) {
            objPlayer.Turn( "RIGHT" );
        }

        if ( gameState.startTimer >= 0 )
        {
            gameState.startTimer--;
        }
        else
        {
            gameState.timer++;
            objPlayer.Update( gameState.timer );
        }
    },

    Draw: function() {
        // Draw grass 9cc978
        gameState.canvas.fillStyle = "#9cc978";
        gameState.canvas.fillRect( 0, 0, main.settings.width, main.settings.height );
        gameState.canvas.drawImage( gameState.background, 0, 0 );

        if ( gameState.startTimer > 0 &&
            ( gameState.startTimer >= 40 && gameState.startTimer < 50 ) ||
            ( gameState.startTimer >= 20 && gameState.startTimer < 30 ) ||
            ( gameState.startTimer >= 0 && gameState.startTimer < 10 ) 
            )
        {
            gameState.canvas.font = "30px Arial";
            gameState.canvas.fillStyle = "#ffffff";
            gameState.canvas.fillText( "Level " + gameState.level, gameState.options.width/2 - 50, gameState.options.height/2 );
        }
        else if ( gameState.startTimer < 0 )
        {
            gameState.canvas.font = "20px Arial";
            gameState.canvas.fillStyle = "#ffffff";
            gameState.canvas.fillText( "Level " + gameState.level, 20, 20 );
        }

        // Draw player
        objPlayer.Draw( gameState.canvas );
    },

    ClickPlay: function() {
    }
};
